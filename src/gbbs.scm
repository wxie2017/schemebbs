;;;; gbbs -- create bbs board using guile
;;; Copyright © 2022 Wensheng Xie <wxie@member.fsf.org>
;;;
;;; This file is part of GNU GBBS.
;;;
;;; GNU GBBS is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; GNU GBBS is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU gbbs.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gbbs)
  #:use-module (ice-9 ftw)
  #:use-module (ice-9 getopt-long)
  #:use-module (ice-9 local-eval)
  #:use-module (ice-9 rdelim)
  #:use-module (gbbs utils)
  #:export (main))

(define +dir-data+ "data")
(define +dir-sexp+ (string-append +dir-data+ "/sexp"))
(define +dir-html+ (string-append +dir-data+ "/html"))
(define *all-boards* '())
(define *html*
  "<!DOCTYPE HTML PUBLIC \"ISO/IEC 15445:2000//DTD HyperText Markup Language//EN\">
  <HTML>
  <HEAD>
  <TITLE>/~A/ - SchemeBBS</TITLE>
  <META content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">
  <META name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <LINK rel=\"icon\" href=\"/static/favicon.ico\" type=\"image/png\">
  <LINK href=\"/static/styles/default.css\" rel=\"stylesheet\" type=\"text/css\"></HEAD>
  <BODY>
  <H1>~A</H1>
  <P class=\"nav\">frontpage - <A href=\"/~A/list\">thread list</A> - <A href=\"#newthread\">new thread</A> - <A href=\"/~A/preferences\">preferences</A> - <A href=\"http://textboard.org\">?</A></P>
  <HR>
  <H2 id=\"newthread\">New Thread</H2>
  <FORM action=\"/~A/post\" method=\"post\">
  <P class=\"newthread\"><LABEL for=\"titulus\">Headline</LABEL><BR>
  <INPUT type=\"text\" name=\"titulus\" id=\"titulus\" size=\"78\" maxlength=\"78\" value=\"\"><BR>
  <LABEL for=\"epistula\">Message</LABEL><BR>
  <TEXTAREA name=\"epistula\" id=\"epistula\" rows=\"12\" cols=\"77\"></TEXTAREA><INPUT type=\"hidden\" name=\"ornamentum\" value=\"3b3738ae1c9295d272cec84ecf7af5c8\"><BR>
  <INPUT type=\"submit\" value=\"POST\"></P>
  <FIELDSET class=\"comment\"><LEGEND>do not edit these</LEGEND>
  <P><INPUT type=\"text\" name=\"name\" class=\"name\" size=\"11\"><BR>
  <TEXTAREA name=\"message\" class=\"message\" rows=\"1\" cols=\"11\"></TEXTAREA></P></FIELDSET></FORM>
  <HR>
  <P class=\"footer\">gbbs.scm + <A href=\"https://www.gnu.org/software/guile/\">GNU Guile</A> + <A href=\"https://mitpress.mit.edu/sites/default/files/sicp/index.html\">SICP</A> + Satori Mode</P></BODY></HTML>")


(define (show-help)
  (display "Usage: gbbs board-name-1 [board-name-2] [...]
At least BOARD-NAME-1 should be provided. Otherwise, display this help messages.

Report bugs to wxie@member.fsf.org.

"))


(define (check-and-mkdir dir)
  "create DIR if not existing"
  (catch
    'system-error
    (lambda ()
      (mkdir dir))
    (lambda stuff
      (let ((errno (system-error-errno stuff)))
        (cond
         ((= errno EACCES)
          (simple-format #t "You're not allowed to create ~A." dir))
         ((= errno EEXIST)
          (simple-format #t "~A already exists." dir))
         (#t
          (display (strerror errno))))
        (newline)
        (exit errno)))))


(define (prepare-directory)
  "create base directory for boards"
  (check-and-mkdir +dir-data+)
  (check-and-mkdir +dir-sexp+)
  (check-and-mkdir +dir-html+))


(define (create-index-sexp index board)
  "create an sexp INDEX file in BOARD"
  (open-file index "w")
  (call-with-output-file index
    (lambda (port)
      (display "()" port))))


(define (create-index-html index board)
  "create an html INDEX file in BOARD"
  (open-file index "w")
  (call-with-output-file index
    (lambda (port)
      (simple-format port *html*
                     board
                     board
                     board
                     board
                     board))))


(define (create-boards boards)
  "create boards for board list BOARDS."
  (prepare-directory)
  (let ((n (length boards)))
    (do ((i 1 (1+ i)))
        ((= i n))
      (let ((board (list-ref boards i)))
        (cond ((member board *all-boards*)
               (simple-format #t "board ~A already exists." board))
              (else
               (check-and-mkdir (string-append +dir-sexp+ "/" board))
               (check-and-mkdir (string-append +dir-html+ "/" board))
               (create-index-sexp (string-append +dir-sexp+ "/" board "/" "index") board)
               (create-index-html (string-append +dir-html+ "/" board "/" "index") board)
               (set! *all-boards* (cons board *all-boards*))))))))



;;;
;;; Entry point.
;;;

(define (main)

  (let ((boards (program-arguments)))  ; (command-line)
    (cond ((> (length boards) 1)
           (create-boards boards))
          (else
           (show-help)
           (exit 0))))

  ;; Forever execute
  (catch-gbbs-error
   (while #t
     (display "hello (gbbs main)\n")
     (exit 0))))
