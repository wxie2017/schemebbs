;;;; config.scm -- variables defined at configure time
;;; Copyright © 2022 Wensheng Xie <wxie@member.fsf.org>
;;;
;;; This file is part of GNU GBBS.
;;;
;;; GNU GBBS is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; GNU GBBS is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Gbbs.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gbbs config))

(define-public config-package-name "GNU Gbbs")
(define-public config-package-version "0.0.1")
(define-public config-package-string "GNU Gbbs 0.0.1")
(define-public config-package-bugreport "bug-gbbs@gnu.org")
(define-public config-package-url "https://www.gnu.org/software/gbbs/")

;;;
;;; Runtime configuration
;;;

(define-public config-debug
  ;; Trigger the display of Guile stack traces on errors.
  (getenv "GBBS_DEBUG"))
